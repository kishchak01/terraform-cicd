terraform {
  backend "s3" {
    bucket = "terraform-backend-prod.xyz"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "terraform-state-locking"
  }
}