# Creating EC2 Instance.check "

resource "aws_instance" "web_server01" {
  ami = "ami-0230bd60aa48260c6"
  instance_type = "t2.micro"
  subnet_id = var.sn
  key_name = "iamkishor-linux"
  security_groups = [var.sg]
  availability_zone = "us-east-1a"
  tags = {
    Name = "web_server01"
  }
}